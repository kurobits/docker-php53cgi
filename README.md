# Docker PHP 5.3 CGI Image #

Why oh why would you want to run PHP 5.3 when its completely unsupported!?!? Life isn't easy and unfortunately you might be in a position where you need to run an old app which was never ported to a modern PHP version. Whatever the reason if that is the case then  this image will help you. In a Jiffy you can setup and Nginx or Apache container and link it to this container and run legacy apps. Lets see how.

## How to use ##

To use this image you need your old app and a docker-compose.yml file. Lets assume the following structure for your app:

```
#!txt

├── app
│   └── index.php
├── default.conf
├── docker-compose.yml
└── .env


```

Your NGinx configuration wound live in the file **default.conf**

```
#!nginx

server {
    listen  80;

    # this path MUST be exactly as docker-compose.fpm.volumes,
    # even if it doesn't exists in this dock.
    root /var/www/app;

    location / {
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/.+\.php(/|$) {
        fastcgi_pass php53cgi:9000;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }
}
```

Your **docker-composer.yml** would look like this:


```
#!yaml

version: "2"

services:
  nginx:
    image: nginx:alpine
    ports:
      - "8080:80"

    volumes:
     - ./default.conf:/etc/nginx/conf.d/default.conf

    links:
      - php53cgi

  php53cgi:
    image: easytech/php53cgi
    volumes:
      - ./app:/var/www/app

```

And for neatness your **.env** file would have the project name to something like this:

```
#!bash

COMPOSE_PROJECT_NAME=LegacyApp

```

Finally in the **app** directory you should have a php like like this **app/index.php**:


```
#!php


<?php

  phpinfo();

```

To do the magic just do:


```
#!bash

$ docker-compose up -d
```

And voilà! If you point your browser to http://localhost:8080 you should see PHP 5.3 info output.!

![phpinfo.png](https://bitbucket.org/repo/6BpyyG/images/4142684343-phpinfo.png)